# Version 3.3.1

## Finished

* Remove `loadIocsh`, which simply duplicates `runScript` for some reason. (Done by Krisztian f02d2c88ffd802f78fa61795486d1bd5ed13e96a)
* Fix the metadata for the wrapper e.g. the url should not fetch origin, but the first remote. FIXED 57dc892bfb29c8ddad82baf2d2f287a766800dca
* Not sure why the description fails for the wrapper, however. Aha: the ifeq in CONFIG_E3_MAKEFILE should compare $$? not $?. FIXED 57dc892bfb29c8ddad82baf2d2f287a766800dca
* `make build` followed by `make cellinstall` should not do a rebuild. This is done because
  `INSTBASE` changes, and for `make cellinstall` we should ignore that. FIXED (require/2f2016179004eae6811f6f3a5567a0db1fd8b81e)
* Allow SKIP_TEST FIXED 3590a3a8ced658251a333c59396cdbe836b418a4
* `make patch` does not respect build numbers: FIXED 7388f41d19230141e5c54ed2130109d30e52514c
* Fix the install permissions for cellMods FIXED aa4749f887ed92fb90f577326c9e756fadbc081b
* `cellinstall` should write the version of base/require! b9168c9a3479bb4efe3df1e7bbfdb667903aa5ab
* require gets its version from CONFIG_MODULE, so why is it specified in RELEASE? This is confusing. ac9ec7b6e73dc5968b5500f8daf93155fb4699fe
* Remove `rm -rf cellMods` from `celluninstall` target; this is not a great idea if we are using multiple modules installed in the same place. 598cf611260fb40780db204cdc814f7c3a0e305e  
* Go through and figure out if we should remove all of the `sudo` usage. 922905e247d6f6ac309b0909f994576f9fedc2b5

## Not done

* Remove more vxWorks/3.x code in e.g. runScript.c and expr.c
* Also, we need to be able to test once it is installed. This would fit with the iterating thing that I had made.
* The auto-load build number thing doesn't seem to work correctly with build descriptors
* Figure out how to fix the fact that asyn, on build, installs devEpics.dbd *** IMPORTANT <=== Needs a patch on EPICS base
* `make cellinstall` does not install vendor libraries anywhere.
* Moreover, vlibs are installed with no reference to `$(T_A)`, which isn't a good idea! One possible idea:
  we could move the `vlibs` target into driver.makefile instead of letting it reside outside in the 
  module makefile. This needs to be discussed.
* module_DB vs module_DIR: DIR includes trailing slash, DB does not. Fix?
* Should there be an error message if non-existent versions of e.g. sequencer are specified?
* base/require version should be added to shared library object; this is needed for cellinstall but should
  more generally be used on startup to check that the library was compiled with the running version of base/require.